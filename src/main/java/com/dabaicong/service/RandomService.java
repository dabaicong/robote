package com.dabaicong.service;

import org.springframework.stereotype.Service;

import java.util.Random;

/**
 * Copyright(c) 2016 dabaicong Co., Ltd.
 * All right reserved.
 * Created by dale on 2016/5/31.
 */
@Service
public class RandomService {

    /**
     * 返回骰子的点数和
     *
     * @param number 骰子有几个面
     * @param count  骰子有几个
     * @param add    在加上几
     * @return 返回count*number+add
     */
    public String getRandom(int number, int count, int add) throws Exception {

        if ( number < 2 || number > 100 ) {
            throw new Exception("骰子面数不正确");
        }
        if ( count < 1 || count > 10 ) {
            throw new Exception("骰子个数不正确");
        }
        int result = 0 ;
        
        String randomString = "(";
        for (int i=0;i<count;i++){
            Random r = new Random(System.currentTimeMillis());
            int oneValue = r.nextInt(number);
            randomString = randomString +oneValue;
            if(i<count-1)  randomString = randomString+ ",";
            result += oneValue;
        }
        
        result+=add;
        randomString = randomString + ")+"+add+"="+result;
        return randomString;
    }
}
