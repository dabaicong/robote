package com.dabaicong.common;

/**
 * Copyright(c) 2016 dabaicong Co., Ltd.
 * All right reserved.
 * Created by dale on 2016/5/31.
 */
public enum ErrorCode {
    Success("0","成功"),
    Fail("2","失败")
    ;

    public String value;

    public String memo;

    ErrorCode(String value, String memo) {
        this.value = value;
        this.memo = memo;
    }

    public static ErrorCode getErrorCode(String value){
        ErrorCode[] errorCode=ErrorCode.values();
        for(ErrorCode error:errorCode){
            if(error.value.equals(value)){
                return error;
            }
        }
        return null;
    }

    public String getValue() {
        return value;
    }

    public String getMemo() {
        return memo;
    }
}
