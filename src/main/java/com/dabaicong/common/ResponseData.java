package com.dabaicong.common;

import net.sf.json.JSONObject;

/**
 * Copyright(c) 2016 dabaicong Co., Ltd.
 * All right reserved.
 * Created by dale on 2016/5/31.
 */
public class ResponseData {

    public String rs;

    public String tip;

    public String end;

    public String getRs() {
        return rs;
    }

    public void setRs(String rs) {
        this.rs = rs;
    }

    public String getTip() {
        return tip;
    }

    public void setTip(String tip) {
        this.tip = tip;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
    
    /**
     * 功       能: <br/>
     * 作       者: 刘冲<br/>
     * 创建日期: 2016年6月1日<br/>
     * 修  改  者: mender<br/>
     * 修改日期: modifydate<br/>
     * @return
     * @see java.lang.Object#toString()
     * 
     */
    @Override
    public String toString() {
    	// TODO Auto-generated method stub
    	JSONObject jsonObject = JSONObject.fromObject(this);
    	return jsonObject.toString();
    }
    
}
