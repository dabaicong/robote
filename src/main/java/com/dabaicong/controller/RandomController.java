package com.dabaicong.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.dabaicong.common.ResponseData;
import com.dabaicong.service.RandomService;

/**
 * Copyright(c) 2016 dabaicong Co., Ltd.
 * All right reserved.
 * Created by dale on 2016/5/31.
 */
@Controller
public class RandomController {

    @Autowired
    private RandomService randomService;


    @RequestMapping(value = "/talk", method = RequestMethod.POST)
    public @ResponseBody
    String rand(
            @RequestParam(value = "content" ) String value,
                @RequestParam(value = "nickname") String nickname) {
    	//value = rr 3d4 5     rr 3d4
        ResponseData rd = new ResponseData();
        rd.setEnd(0+"");
        rd.setRs("1");

        int number = 2;
        int count = 1;
        int add = 0;
        String utfNickname = new String(nickname.getBytes("Unicode"),"UTF-8");
        String utfString = new String(" 掷出了".getBytes(),"UTF-8");
        if ( !value.contains("d") ){
            rd.setTip("输入错误！");
            return rd.toString() ;
        }
        if ( !value.contains("rr") ){
            rd.setTip("无法识别！");
            return rd.toString() ;
        }
        String[] values = value.split(" ");
        if(values.length==2){
        	add = 0;
        }else if(values.length==3) {
        	add = Integer.parseInt(values[2]);
        }
        //rr 3d6 4 , number=6 ,count=3 add=4
        try {
            count = Integer.parseInt(values[1].split("d")[0]);
            number = Integer.parseInt(values[1].split("d")[1]);
            rd.setTip("@"+utfNickname+utfString+randomService.getRandom(number,count,add));
        } catch (Exception e) {
            rd.setTip(e.getMessage());
        }

        return rd.toString() ;
    }

}
